const link = "http://localhost/";

function resetForm() {
  document.getElementById("updateForm").reset(); //clears form data by setting all fields to empty string
}

fetch(link + "REST_API/api/read.php", {
  // headers: {
  //   "ngrok-skip-browser-warning": true,
  // },
})
  .then(function (response) {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Error retrieving data.");
    }
  })
  .then(function (data) {
    if (Array.isArray(data.data)) {
      var html = "";
      data.data.forEach(function (item, index) {
        html += `
          <tr>
            <td>${index + 1}</td>
            <td>${item.name}</td>
            <td>${item.email}</td>
            <td>${item.phone}</td>
            <td>
              <button type="button" class="btn btn-primary" onclick="updateItem(this, '${
                item.id
              }')">Update User</button>
            </td>
            <td>
              <button type="button" class="btn btn-danger" onclick="deleteItem(this, ${
                item.id
              })">Delete</button>
            </td>
          </tr>`;
      });
      $("#dataContainer").html(html);
    } else {
      console.log("Invalid data format:", data);
    }
  })
  .catch(function (error) {
    console.log("Error:", error);
  });

let mainElement;
let ID;

function updateItem(element, id) {
  mainElement = element;
  ID = id;

  var updateName = $(element).parent().prev().prev().prev().text();
  var updateEmail = $(element).parent().prev().prev().text();
  var updatePhone = $(element).parent().prev().text();

  $("#updateName").val(updateName);
  $("#updateEmail").val(updateEmail);
  $("#updatePhone").val(updatePhone);

  $("#dialogForm").show();
}

function insert() {
  $("#dialogForm").show();
}

document
  .getElementById("updateForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    var id = ID;
    var name = document.getElementById("updateName").value;
    var email = document.getElementById("updateEmail").value;
    var phone = document.getElementById("updatePhone").value;

    if (ID == undefined) {
      // debugger;
      // Perform the save request
      fetch(link + "REST_API/api/create.php", {
        method: "POST",
        // headers: {
        //   "ngrok-skip-browser-warning": true,
        // },
        body: JSON.stringify({ name: name, email: email, phone: phone }),

        // body: { name: name, email: email, phone: phone }
      })
        .then(function (response) {
          if (response.ok) {
            fetch();
            alert("User Insert Successfully");
            closeButton();
            resetForm();
          } else {
            throw new Error("Error updating data.");
            resetForm();
          }
        })
        .then(function (data) {
          console.log("Item Insert:", data);
          document.getElementById("dialogForm").style.display = "none";
        })
        .catch(function (error) {
          console.log("Error:", error);
        });
    } else {
      // Perform the update request
      fetch(link + "REST_API/api/update.php?id=" + id, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          // "ngrok-skip-browser-warning": true,
        },
        body: JSON.stringify({ name: name, email: email, phone: phone }),
      })
        .then(function (response) {
          if (response.ok) {
            $(mainElement).parent().prev().prev().prev().text(name);
            $(mainElement).parent().prev().prev().text(email);
            $(mainElement).parent().prev().text(phone);
            mainElement = undefined;
            alert("User Update Successfully");
          } else {
            throw new Error("Error updating data.");
          }
        })
        .then(function (data) {
          console.log("Item updated:", data);
          document.getElementById("dialogForm").style.display = "none";
          resetForm();
        })
        .catch(function (error) {
          console.log("Error:", error);
          resetForm();
        });
    }
  });

function deleteItem(element, id) {
  mainElement = element;
  let confirmDelete = confirm(
    "Are you sure you want to delete this user? This action cannot be undone."
  );

  if (confirmDelete) {
    // Perform the delete request
    fetch(link + "REST_API/api/delete.php?id=" + id, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        // "ngrok-skip-browser-warning": true,
      },
    })
      .then(function (response) {
        if (response.ok) {
          $(mainElement).closest("tr").remove();
        } else {
          throw new Error("Error deleting data.");
        }
      })
      .then(function (data) {
        console.log("Item deleted:", data);
        mainElement = undefined;
      })
      .catch(function (error) {
        console.log("Error:", error);
      });
  }
}

function closeButton() {
  document.getElementById("dialogForm").style.display = "none";
  resetForm();
}

$("#updatePhone").on("input", function () {
  var phone = $(this).val();

  if (phone.length !== 10) {
    $(this).addClass("is-invalid");
  } else {
    $(this).removeClass("is-invalid");
  }
});
