<?php

    // include('code/config.php');
    include('code/function.php');

    $requestMethod = $_SERVER["REQUEST_METHOD"];

    if ($requestMethod === 'OPTIONS') {
        // Allow the preflight request
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Origin: *"); // You can replace * with specific origins
        header("Access-Control-Allow-Methods: DELETE");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
        exit();
    }

    if ($requestMethod === 'DELETE') {
            
            $deleteUser = deleteUser($_GET); // Get the SINGLE user RECORD from the database by the user id.
            echo $deleteUser;
            
        } else {
            // Handle invalid request method
            $data = [
                'status' => '405',
                'message' => $requestMethod . ' Method Not Allowed'
            ];
            http_response_code(405); // Set the HTTP response code to 405
            echo json_encode($data);
    }
?>