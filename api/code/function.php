<?php
require('config.php');

// 1.0 FUNCTION getRecord(): Display ALL record from database
function getRecord()
{
    global $conn; // global variable for db connection

    $query = "SELECT * FROM users";
    $stmt = $conn->prepare($query);
    $stmt->execute();

    if ($stmt) {
        if ($stmt->rowCount() > 0) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $data = [
                'status' => '200',
                'message' => 'Successful',
                'data' => $result
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => '404',
                'message' => 'No results!'
            ];
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => '500',
            'message' => 'Error executing query'
        ];
        return json_encode($data);
    }
}

// 1.1 FUNCTION getUser(): Display SINGLE record from database
function getUser($userParam) {
    global $conn; // global variable for PDO connection

    if ($userParam['id'] == null) {
        return error422('Enter User id');
    }

    $query = "SELECT * FROM users WHERE id = :userId LIMIT 1";
    $statement = $conn->prepare($query);
    $statement->bindValue(':userId', $userParam['id'], PDO::PARAM_STR);
    $statement->execute();

    if ($statement->rowCount() == 1) {
        $res = $statement->fetch(PDO::FETCH_ASSOC);

        $data = [
            'status' => '200',
            'message' => 'User fetch Successfully',
            'data' => $res
        ];
        return json_encode($data);
    } else {
        $data = [
            'status' => '404',
            'message' => 'No User Found',
        ];
        return json_encode($data);
    }
}

// FUNCTION storeRecord(): Store record to database
function error422($message) {
    $data = [
        'status' => '422',
        'message' => $message,
    ];
    http_response_code(422); // Set the HTTP response code to 422
    echo json_encode($data);
    exit(); // Stop execution of the script
}

function storeRecord($userInput) {
    global $conn; // global variable for db connection

    $name = trim($userInput['name']);
    $email = trim($userInput['email']);
    $phone = trim($userInput['phone']);

    if (empty($name)) {
        return error422('Enter Your Name');
    } elseif (empty($email)) {
        return error422('Enter Your Email');
    } elseif (empty($phone)) {
        return error422('Enter Your Phone');
    } else {
        try {
            $query = "INSERT INTO users (name, email, phone) VALUES (:name, :email, :phone)";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':phone', $phone);
            $result = $stmt->execute();

            if ($result) {
                $data = [
                    'status' => '201',
                    'message' => 'User Input Success',
                ];
                return json_encode($data);
            } else {
                $data = [
                    'status' => '500',
                    'message' => 'Error executing query',
                ];
                return json_encode($data);
            }
        } catch (PDOException $e) {
            $data = [
                'status' => '500',
                'message' => 'Database error: ' . $e->getMessage(),
            ];
            return json_encode($data);
        }
    }
}

// FUNCTION updateRecord(): UPDATE record to database
function updateRecord($userInput, $userParam) {
    global $conn; // global variable for PDO connection

    if (!isset($userParam['id'])) {
        return error422('User id not found in the URL');
    } elseif ($userParam['id'] == null) {
        return error422('Enter the User ID');
    }

    $userId = $userParam['id'];

    // $userId = trim($userParam['id']);
    $name = trim($userInput['name']);
    $email = trim($userInput['email']);
    $phone = trim($userInput['phone']);

    if (empty($name)) {
        return error422('Enter Your Name');
    } elseif (empty($email)) {
        return error422('Enter Your Email');
    } elseif (empty($phone)) {
        return error422('Enter Your Phone');
    } else {
        try {
            $query = "UPDATE users SET name = :name, email = :email, phone = :phone WHERE id = :userId LIMIT 1";
            $statement = $conn->prepare($query);
            $statement->bindValue(':name', $name, PDO::PARAM_STR);
            $statement->bindValue(':email', $email, PDO::PARAM_STR);
            $statement->bindValue(':phone', $phone, PDO::PARAM_STR);
            $statement->bindValue(':userId', $userId, PDO::PARAM_STR);
            $result = $statement->execute();

            if ($result) {
                $data = [
                    'status' => '200',
                    'message' => 'User Update Success',
                ];
                return json_encode($data);
            } else {
                $data = [
                    'status' => '500',
                    'message' => 'Error executing query',
                ];
                return json_encode($data);
            }
        } catch (PDOException $e) {
            $data = [
                'status' => '500',
                'message' => 'Database error: ' . $e->getMessage(),
            ];
            return json_encode($data);
        }
    }
}

// FUNCTION deleteUser(): DELETE user from database
function deleteUser($userParam) {
    global $conn;

    if (!isset($userParam['id'])) {
        return error422('User id not found in the URL');
    } elseif ($userParam['id'] == null) {
        return error422('Enter the User ID');
    }

    $userId = $userParam['id'];

    $query = "DELETE FROM users WHERE id=:userId LIMIT 1";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(':userId', $userId);
    $result = $stmt->execute();

    if($result) {
        $data = [
            'status' => '200',
            'message' => 'Delete Success',
        ];
        return json_encode($data);
    } else {
        $data = [
            'status' => '404',
            'message' => 'User not found',
        ];
        return json_encode($data);
    }
}


?>