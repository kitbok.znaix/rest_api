<?php

    
    include('code/function.php');
    
    header('Access-Control-Allow-Methods:POST'); // ALLOW CORS OPTIONS REQUESTS
    header('Content-Type: application/json');


    $requestMethod = $_SERVER["REQUEST_METHOD"];

    $storeRecord = '';
    
    if($requestMethod === 'POST') {

        $inputData = json_decode(file_get_contents("php://input"), true);
        if(empty($inputData)){
            echo('no data found'); // exit if data is empty.
        } else {
            $storeRecord = storeRecord($inputData);
        }

        echo $storeRecord;

    } else {
        // Handle invalid request method
        $data = [
            'status' => '405',
            'message' => $requestMethod . ' Method Not Allowed'
        ];
        http_response_code(405); // Set the HTTP response code to 405
        echo json_encode($data);
    }
?>
