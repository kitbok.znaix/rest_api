<?php
    include('code/function.php');

    header('Access-Control-Allow-Methods:PUT'); // ALLOW CORS OPTIONS REQUESTS
    header('Content-Type: application/json');

    $requestMethod = $_SERVER["REQUEST_METHOD"];

    if ($requestMethod === 'OPTIONS') {
        // Allow the preflight request
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Origin: *"); // You can replace * with specific origins
        header("Access-Control-Allow-Methods: PUT");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
        exit();
    }

    $updateRecord ='';

    if($requestMethod === 'PUT') {

        $inputData = json_decode(file_get_contents("php://input"), true);
        if(empty($inputData)){
            echo('no data found'); // exit if data is empty.
        } else {
            $updateRecord = updateRecord($inputData, $_GET);
        }

        echo $updateRecord;

    } else {
        // Handle invalid request method
        $data = [
            'status' => '405',
            'message' => $requestMethod . ' Method Not Allowed'
        ];
        http_response_code(405); // Set the HTTP response code to 405
        echo json_encode($data);
    }

?>