<?php

    // include('code/config.php');
    include('code/function.php');

    header('Access-Control-Allow-Methods:GET'); // ALLOW CORS OPTIONS REQUESTS
    header('Content-Type: application/json');

    $requestMethod = $_SERVER['REQUEST_METHOD']; // REQUEST METHOD - GET, PUT, PATCH, POST, DELETE

    if ($requestMethod === 'GET') {
            
        if(isset($_GET['id'])) {
            $user = getUser($_GET); // Get the SINGLE user RECORD from the database by the user id.
            echo $user;

        } else {
            $recordList = getRecord();
            echo $recordList; // echo the JSON string
        }
            
        } else {
            // Handle invalid request method
            $data = [
                'status' => '405',
                'message' => $requestMethod . ' Method Not Allowed'
            ];
            http_response_code(405); // Set the HTTP response code to 405
            echo json_encode($data);
    }
?>